# GitLab CI: When
([Source](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/))

---

## WHEN
Comme pour les directives *only* et *except*, la directive *when* est une contrainte sur l’exécution de la tâche. Il y a quatre modes possibles :

- **on_success** : le job sera exécuté uniquement si tous les jobs du stage précédent sont passés
- **on_failure** : le job sera exécuté uniquement si un job est en échec
- **always** : le job s’exécutera quoi qu’il se passe (même en cas d’échec)
- **manual** : le job s'exécutera uniquement par une action manuelle

``` yaml
stages:
  - build
  - test
  - report
  - clean

job:build:
  stage: build
  script:
    - - echo "Performing Builds ...."

job:test:
  stage: test
  script:
    - echo "Performing Tests ...."
  when: on_success # s'exécutera uniquement si le job `job:build` passe

job:report:
  stage: report
  script:
    - echo "Producing Reports ...."
  when: on_failure # s'exécutera si le job `job:build` ou `job:test` ne passe pas

job:clean:
  stage: clean
  script:
    - echo "Cleaning Up always  ...." # s'exécutera quoi qu'il se passe
  when: always
  ``` 

#### ALLOW_FAILURE
Cette directive permet d’accepter qu’un job échoue sans faire échouer la pipeline.

```  yaml
stages:
  - build
  - test
  - report
  - clean

...

stage: clean
  script:
    - make clean
    when: always
    allow_failure: true # Ne fera pas échouer la pipeline
...
``` 
